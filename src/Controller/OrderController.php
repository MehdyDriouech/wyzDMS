<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Annotation\Route;


class OrderController extends Controller
{

    private $DMSreturnCode;
    private $DMSorderNumber;
    private $DMSreturnMessage;
    private $DMScustomerAccount;
    private $DMSorderType;

    /**
     * @Route(
     *     name="order",
     *     path="/api/order",
     *     methods={"POST"}
     * )
     */
    public function beautifulProcessOrderAction(Request $request)
    {
        $json = $this->parseInputAsJson($request->getContent());
        if(!$json){
            return New Response("request body must be a json", 400);
        }

        $firstObject = urlencode($this->getFirstObject($json['customerCode'], $json['orderCommentsComplete'], $json['adressComplete']));
        $response_content = $this->curlInteraction("http://46.218.48.6:18802/WebService?id=RR_WS_PTS_CUSTORDER&xml=".$firstObject);
        if(!$response_content)
        {
            return new Response("Step 1 Failed", 404);
        }
        $this->parseResponseStep1($response_content); //   $response_content
        if($this->DMSorderNumber == null || $this->DMSreturnCode != '00'){
            return new Response($this->DMSreturnMessage, 404);
        }

        $secondObject = urlencode($this->getSecondObject($this->DMSorderNumber, $json['customerCode'], $json['productId'], $json['productQuantity'], $json['orderCommentsComplete']));
        $response_content = $this->curlInteraction("http://46.218.48.6:18802/WebService?id=RR_WS_PTS_CUSTORDER&xml=".$secondObject);
        if(!$response_content)
        {
            return new Response("Step 2 Failed", 404);
        }

        $this->parseResponseStep2($response_content); //   $response_content
        if($this->DMSorderNumber == null || $this->DMSreturnCode != '00'){
            return new Response($this->DMSreturnMessage, 404);
        }

        $thirdObject = urlencode($this->getThirdObject($this->DMSorderNumber, $json['customerCode']));
        $response_content = $this->curlInteraction("http://46.218.48.6:18802/WebService?id=RR_WS_PTS_CUSTORDER&xml=".$thirdObject);
        if(!$response_content)
        {
            return new Response("Step 3 Failed", 404);
        }
        $this->parseResponseStep3($response_content); //   $response_content
        if($this->DMSorderNumber == null || $this->DMSreturnCode != '00'){
            return new Response($this->DMSreturnMessage, 404);
        }

        return new Response("La commande ".$this->DMSorderNumber." a été créée", "200");

    }

    private function curlInteraction($url){

        $ch = curl_init($url);
        $username = 'format';
        $password = 'format';

        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response_content = curl_exec($ch);

        $errors = curl_error($ch);

        curl_close($ch);
        $response = null;
        if(!$errors){
            $response = $response_content;
        }
        return $response;
    }

    private function parseResponseStep1($response_content)
    {
        $output = new \SimpleXMLElement($response_content);

        $this->DMSreturnCode =$output->output->returnCode;
        $this->DMSorderNumber = $output->output->orderNumber;
    }

    private function parseResponseStep2($response_content)
    {
        $output = new \SimpleXMLElement($response_content);

        $this->DMSreturnCode=$output->output->returnCode;
        $this->DMSreturnMessage=$output->output->returnMessage;
    }

    private function parseResponseStep3($response_content)
    {

        $output = new \SimpleXMLElement($response_content);

        $this->DMSreturnCode=$output->output->returnCode;
        $this->DMSreturnMessage=$output->output->returnMessage;
        $this->DMSorderNumber=$output->output->orderNumber;
        $this->DMScustomerAccount=$output->output->customerAccount;
        $this->DMSorderType=$output->output->orderType;
    }

    private function getFirstObject($customerCode, $orderCommentsComplete, $AdressComplete){
        return "
        <ws:apv.basket.Basket.openBasket xmlns:ws=\"http://www.reyrey.fr/ws/rr_ws_pts_custorder/\">
             <input>
                  <basketHeader>
                        <customerAccount>$customerCode</customerAccount>
                        <customerAccountType>CLI</customerAccountType>
                        <orderType>CMDE_MANUELLE</orderType>
                        <author>41</author>
                        <customerMark>Repere client</customerMark>
                        <note>NOTE</note>
                        <orderComment>$orderCommentsComplete</orderComment>
                        <deliveryAdress>$AdressComplete</deliveryAdress>
                        <origin>ORI</origin>
                  </basketHeader>
            </input>
        </ws:apv.basket.Basket.openBasket>
        ";
    }

    private function getSecondObject($DMSorderNumber, $customerCode, $productId, $productQuantity, $orderCommentsComplete){
        return "<ws:apv.basket.Basket.addBasketLine  xmlns:ws=\"http://www.reyrey.fr/ws/rr_ws_pts_custorder/\">
            <input>
                <basketHeader>
                    <orderNumber>$DMSorderNumber</orderNumber>
                    <customerAccount>$customerCode</customerAccount>
                    <orderType>CMDE_MANUELLE</orderType>
                </basketHeader>
                <basketLine>
                    <part>
                        <id>$productId</id>
                    </part>
                    <initialQuantity>$productQuantity</initialQuantity>
                    <lineComment>$orderCommentsComplete</lineComment>
                </basketLine>
            </input>
            </ws:apv.basket.Basket.addBasketLine>
        ";
    }

    private function getThirdObject($DMSorderNumber, $customerCode){
        return "
        <ws:apv.basket.Basket.closeBasket  xmlns:ws=\"http://www.reyrey.fr/ws/rr_ws_pts_custorder/\"> 
            <input> 
                <basketHeader> 
                    <orderNumber>$DMSorderNumber</orderNumber>
                    <customerAccount>$customerCode</customerAccount> 
                    <orderType>CMDE_MANUELLE</orderType> 
                </basketHeader>
            </input> 
        </ws:apv.basket.Basket.closeBasket>
        ";
    }

    private function parseInputAsJson($json_string){
        $response = null;
        if($json_string && !empty($json_string)) {

            //decode json to get a readable file + get a specific part of this file (products).
            $json = json_decode($json_string, true);

            $response = [];
            try {
                $products = $json['order']['order_lines']['products'];

                //get the infos we need.
                $response['orderReference'] = $json['order']['order_reference'];
                $response['customerCode'] = $json['customer']['customer_code'];
                $response['customerName'] = $json['customer']['customer_delivery']['customer_delivery_name'];
                $response['customerAdress'] = $json['customer']['customer_delivery']['customer_delivery_address_line_1'];
                $response['customerZip'] = $json['customer']['customer_delivery']['customer_delivery_zip_code'];
                $response['customerCity'] = $json['customer']['customer_delivery']['customer_delivery_city'];
                $response['customerCountry'] = $json['customer']['customer_delivery']['customer_delivery_country']['customer_delivery_country_name'];
                $response['productQuantity'] = $products[0]['product_quantity'];
                $response['productId'] = $products[0]['product_ean'];
                $response['orderComments1'] = $json['order']['order_comments']['order_comments_1'];
                $response['orderComments2'] = $json['order']['order_comments']['order_comments_2'];
                $response['orderComments3'] = $json['order']['order_comments']['order_comments_3'];

                //Concat info for lisibility
                $response['adressComplete'] = $response['customerAdress'] . " " . $response['customerZip'] . " " . $response['customerCity'] . " " . $response['customerCountry'];
                $response['orderCommentsComplete'] = $response['orderComments1'] . " " . $response['orderComments2'] . " " . $response['orderComments3'];
            } catch (\Exception $e) {

                $response = null;
            }
        }
        return $response;
    }
}